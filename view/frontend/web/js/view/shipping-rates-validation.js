/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpDHLShipping
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-rates-validation-rules',
        'Webkul_MpDHLShipping/js/model/shipping-rates-validator',
        'Webkul_MpDHLShipping/js/model/shipping-rates-validation-rules'
    ],
    function (
        Component,
        defaultShippingRatesValidator,
        defaultShippingRatesValidationRules,
        mpdhlShippingRatesValidator,
        mpdhlShippingRatesValidationRules
    ) {
        'use strict';
        defaultShippingRatesValidator.registerValidator('mpdhl', mpdhlShippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('mpdhl', mpdhlShippingRatesValidationRules);

        return Component;
    }
);
